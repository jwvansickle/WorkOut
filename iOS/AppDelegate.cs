﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace WorkOut.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            var dbName = LocalFileHelper.GetLocalFilePath("workout.db3");
            LoadApplication(new App(dbName));

            return base.FinishedLaunching(app, options);
        }
    }
}
