﻿using WorkOut.Repositories;
using Xamarin.Forms;

namespace WorkOut
{
    public partial class App : Application
    {
        public static ExerciseRepository ExerciseRepo { get; private set; }

        public static WorkoutRepository WorkoutRepo { get; private set; }

        /// <summary>
        /// Just for the UI preview renderer. DO NOT USE
        /// </summary>
        public App()
        {
            InitializeComponent();
        }

        public App(string dbName)
        {
            InitializeComponent();

            ExerciseRepo = new ExerciseRepository(dbName);
            WorkoutRepo = new WorkoutRepository(dbName);

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
