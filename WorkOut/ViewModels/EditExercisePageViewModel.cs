﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using WorkOut.Models;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;

namespace WorkOut.ViewModels
{
    public class EditExercisePageViewModel : INotifyPropertyChanged
    {
        public EditExercisePageViewModel(ExerciseVM vm, INavigation navigation)
        {
            Id = vm.Id;
            Name = vm.Name;
            Repetitions = vm.Repetitions;
            TimeLength = vm.TimeLength;

            Navigation = navigation;
        }

        private int Id { get; set; }

        private string _name;
        public string Name { get { return _name; } set { _name = value; OnPropertyChanged("Name"); } }

        private short _repetitions;
        public short Repetitions { get { return _repetitions; } set { _repetitions = value; OnPropertyChanged("Repetitions"); } }

        private long _timeLength;
        public long TimeLength { get { return _timeLength; } set { _timeLength = value; OnPropertyChanged("TimeLength"); } }

        public ICommand SaveExerciseCommand { get => new Command(SaveExercise); }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly INavigation Navigation;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void SaveExercise()
        {
            await App.ExerciseRepo.UpdateAsync(new Exercise
            {
                Id = Id,
                Name = Name,
                Repetitions = Repetitions,
                TimeLength = TimeLength
            });

            await Navigation.PopAsync();
        }
    }
}
