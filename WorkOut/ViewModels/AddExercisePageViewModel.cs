﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace WorkOut.ViewModels
{
    public class AddExercisePageViewModel : INotifyPropertyChanged
    {
        public AddExercisePageViewModel(ExercisesPageViewModel exercisesPageViewModel, INavigation navigation)
        {
            _exercisesPageViewModel = exercisesPageViewModel;
            _navigation = navigation;
        }

        private string _exerciseName;
        public string ExerciseName { get { return _exerciseName; } set { _exerciseName = value; OnPropertyChanged("ExerciseName"); } }

        private short _repetitions;
        public short Repetitions { get { return _repetitions; } set { _repetitions = value; OnPropertyChanged("Repetitions"); } }

        private long _timeLength;
        public long TimeLength { get { return _timeLength; } set { _timeLength = value; OnPropertyChanged("TimeLength"); } }

        public ICommand CreateExerciseCommand { get => new Command(CreateExercise); }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly ExercisesPageViewModel _exercisesPageViewModel;

        private readonly INavigation _navigation;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void CreateExercise()
        {
            var model = new Models.Exercise
            {
                Name = ExerciseName,
                Repetitions = Repetitions,
                TimeLength = TimeLength
            };

            var newId = await App.ExerciseRepo.CreateAsync(model);
            model.Id = newId;

            _exercisesPageViewModel.Exercises.Add(ExercisesPageViewModel.ExerciseVM.FromModel(model));

            await _navigation.PopAsync();
        }
    }
}
