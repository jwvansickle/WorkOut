﻿using System.Linq;
using static WorkOut.ViewModels.WorkoutsPageViewModel;

namespace WorkOut.ViewModels
{
    public class WorkoutDetailPageViewModel
    {
        public WorkoutVM Workout { get; private set; }

        public string Summary { get => $"{Workout.Exercises.Select(e => e.TimeLength).Sum()} minute workout"; }

        public WorkoutDetailPageViewModel(WorkoutVM workout)
        {
            Workout = workout;
        }
    }
}
