﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using WorkOut.Pages;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;
using static WorkOut.ViewModels.WorkoutsPageViewModel;

namespace WorkOut.ViewModels
{
    public class AddWorkoutPageViewModel : INotifyPropertyChanged
    {
        public AddWorkoutPageViewModel(INavigation navigation)
        {
            Navigation = navigation;
        }

        private string _workoutName;
        public string WorkoutName { get { return _workoutName; } set { _workoutName = value; OnPropertyChanged("WorkoutName"); } }

        public ICommand SelectExercisesCommand { get => new Command(SelectExercises); }

        public ICommand CreateWorkoutCommand { get => new Command(CreateWorkout); }

        public event PropertyChangedEventHandler PropertyChanged;

        private readonly INavigation Navigation;

        private SelectMultipleBasePage<ExerciseVM> exerciseSelectPage;

        private List<ExerciseVM> Exercises { get => App.ExerciseRepo.GetAll().Select(model => ExerciseVM.FromModel(model)).ToList(); }

        private List<ExerciseVM> ExerciseSelections { get => exerciseSelectPage.GetSelection(); }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void SelectExercises()
        {
            exerciseSelectPage = new ExerciseSelectPage(Exercises)
            {
                Title = "Select at least 1"
            };

            await Navigation.PushAsync(exerciseSelectPage);
        }

        private async void CreateWorkout()
        {
            // TODO Verify user input
            var vm = new WorkoutVM
            {
                Name = WorkoutName,
                Exercises = ExerciseSelections
            };

            await App.WorkoutRepo.Create(vm.ToModel());

            await Navigation.PopAsync();
        }
    }
}
