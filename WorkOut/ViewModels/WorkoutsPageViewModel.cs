﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using WorkOut.Models;
using WorkOut.Pages;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;

namespace WorkOut.ViewModels
{
    public class WorkoutsPageViewModel
    {
        public ObservableCollection<WorkoutVM> Workouts { get; private set; } = new ObservableCollection<WorkoutVM>();

        public ICommand AddWorkoutCommand { get => new Command(AddWorkout); }

        public ICommand DeleteWorkoutCommand { get => new Command<WorkoutVM>(DeleteWorkout); }

        public ICommand ItemSelectedCommand { get => new Command<WorkoutVM>(ItemSelected); }

        private readonly INavigation Navigation;

        #region Constructors
        public WorkoutsPageViewModel(INavigation navigation)
        {
            Navigation = navigation;
        }
        #endregion

        #region Public methods
        public async Task RefreshWorkouts()
        {
            Workouts.Clear();

            var models = await App.WorkoutRepo.GetAll();

            var workouts = models.Select(wo => WorkoutVM.FromModel(wo));

            foreach (var wo in workouts)
            {
                Workouts.Add(wo);
            }
        }
        #endregion

        #region Private methods
        private async void AddWorkout()
        {
            var page = new AddWorkoutPage();

            await Navigation.PushAsync(page);
        }

        private async void DeleteWorkout(WorkoutVM wo)
        {
            await App.WorkoutRepo.Delete(wo.ToModel());

            Workouts.Remove(wo);
        }

        private async void ItemSelected(WorkoutVM wo)
        {
            var page = new WorkoutDetailPage(wo);

            await Navigation.PushAsync(page);
        }
        #endregion

        public class WorkoutVM
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public IEnumerable<ExerciseVM> Exercises { get; set; }

            public long TotalWorkoutTime { get { return Exercises.Select(e => e.TimeLength).Sum(); } }

            public string TotalWorkoutTimeUI { get { return Exercises.Select(e => e.TimeLength).Sum() + " mins"; } }

            public long TotalRepetitions { get { return Exercises.Select(e => (long)e.Repetitions).Sum(); } }

            public string TotalRepetitionsUI { get { return Exercises.Select(e => (long)e.Repetitions).Sum() + " reps"; } }

            public static WorkoutVM FromModel(Workout model)
            {
                var exercises = model.Exercises.Select(em => ExerciseVM.FromModel(em));

                return new WorkoutVM
                {
                    Id = model.Id,
                    Name = model.Name,
                    Exercises = exercises
                };
            }

            public Workout ToModel()
            {
                return new Workout
                {
                    Id = Id,
                    Name = Name,
                    Exercises = Exercises.Select(e => e.ToModel()).ToList()
                };
            }
        }
    }
}
