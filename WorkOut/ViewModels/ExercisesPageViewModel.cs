﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WorkOut.Models;

namespace WorkOut.ViewModels
{
    public class ExercisesPageViewModel
    {
        public ObservableCollection<ExerciseVM> Exercises { get; private set; } = new ObservableCollection<ExerciseVM>();

        public async Task RefreshExercises()
        {
            var exercises = await App.ExerciseRepo.GetAllAsync();

            Exercises.Clear();
            foreach (var exercise in exercises)
            {
                Exercises.Add(ExerciseVM.FromModel(exercise));
            }
        }

        public class ExerciseVM
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public short Repetitions { get; set; }
            public string RepetitionsUI { get { return $"{Repetitions} reps"; } }
            public long TimeLength { get; set; }
            public string TimeLengthUI { get { return $"{TimeLength} mins"; } }

            public static ExerciseVM FromModel(Exercise exercise)
            {
                return new ExerciseVM
                {
                    Id = exercise.Id,
                    Name = exercise.Name,
                    Repetitions = exercise.Repetitions,
                    TimeLength = exercise.TimeLength
                };
            }

            public Exercise ToModel()
            {
                return new Exercise
                {
                    Id = Id,
                    Name = Name,
                    Repetitions = Repetitions,
                    TimeLength = TimeLength
                };
            }
        }
    }
}
