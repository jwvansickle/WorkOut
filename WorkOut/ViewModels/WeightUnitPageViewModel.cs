﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WorkOut.ViewModels
{
    public class WeightUnitPageViewModel
    {
        public ObservableCollection<WeightUnit> WeightUnits { get; set; } = new ObservableCollection<WeightUnit>
        {
            new WeightUnit { Name = "Pounds (lbs)" },
            new WeightUnit { Name = "Kilograms (kg)" }
        };

        public WeightUnitPageViewModel()
        {
        }

        public class WeightUnit
        {
            public string Name { get; set; }
        }
    }
}
