﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using WorkOut.Models;

namespace WorkOut.Repositories
{
    public class WorkoutRepository
    {
        private readonly SQLiteAsyncConnection conn;

        public WorkoutRepository(string dbName)
        {
            conn = new SQLiteAsyncConnection(dbName);

            conn.CreateTablesAsync<Workout, WorkoutExercise>().Wait();
        }

        public async Task<int> Create(Workout workout)
        {
            int newId = -1;

            await conn.RunInTransactionAsync((SQLiteConnection conn) =>
            {
                conn.Insert(workout);

                newId = workout.Id;

                // workout.Exercises is ignored, store relationship in intersect table
                foreach (var exercise in workout.Exercises.ToList())
                {
                    var intersect = new WorkoutExercise
                    {
                        WorkoutId = workout.Id,
                        ExerciseId = exercise.Id
                    };

                    conn.Insert(intersect);
                }

                conn.Commit();
            });

            return newId;
        }

        public async Task<IEnumerable<Workout>> GetAll()
        {
            var workouts = new List<Workout>();

            await conn.RunInTransactionAsync((SQLiteConnection conn) =>
            {
                workouts = conn.Table<Workout>().ToList();

                // Need to populate each Workout.Exercises property
                foreach (var wo in workouts)
                {
                    wo.Exercises = new List<Exercise>();
                    var intersects = conn.Table<WorkoutExercise>().Where(we => we.WorkoutId == wo.Id).ToList();

                    foreach (var i in intersects)
                    {
                        var exercise = conn.Table<Exercise>().First(e => e.Id == i.ExerciseId);

                        wo.Exercises.Add(exercise);
                    }
                }

                conn.Commit();
            });

            return workouts;
        }

        public async Task Delete(Workout workout)
        {
            await conn.RunInTransactionAsync((SQLiteConnection conn) =>
            {
                // Delete all exercise relations of the workout
                var workoutExercises = conn.Table<WorkoutExercise>().Where(we => we.WorkoutId == workout.Id).ToList();

                foreach (var we in workoutExercises)
                {
                    conn.Delete(we);
                }

                // Delete the workout
                conn.Delete(workout);

                conn.Commit();
            });
        }

        public async Task<IEnumerable<int>> Search(SearchParameters query)
        {
            var result = await conn.Table<WorkoutExercise>().Where(
                we => 
                we.ExerciseId == query.HasExercise
            ).ToListAsync();

            return result.Select(we => we.WorkoutId);
        }

        public class SearchParameters
        {
            public int HasExercise { get; set; }
        }
    }
}
