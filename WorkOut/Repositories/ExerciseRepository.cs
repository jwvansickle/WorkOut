﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using WorkOut.Models;

namespace WorkOut.Repositories
{
    public class ExerciseRepository
    {
        private readonly SQLiteAsyncConnection conn;

        private readonly SQLiteConnection syncConn;

        public ExerciseRepository(string dbName)
        {
            conn = new SQLiteAsyncConnection(dbName);
            syncConn = new SQLiteConnection(dbName);

            // Create tables
            conn.CreateTableAsync<Exercise>().Wait();
        }

        public IEnumerable<Exercise> GetAll()
        {
            return syncConn.Table<Exercise>();
        }

        public async Task<IEnumerable<Exercise>> GetAllAsync()
        {
            return await conn.Table<Exercise>().ToListAsync();
        }

        public async Task<int> CreateAsync(Exercise exercise)
        {
            var result = await conn.InsertAsync(exercise);

            return result;
        }

        public async Task DeleteAsync(Exercise exercise)
        {
            await conn.DeleteAsync(exercise);
        }

        public async Task UpdateAsync(Exercise exercise)
        {
            await conn.UpdateAsync(exercise);
        }
    
        public async Task<Exercise> GetAsync(int id)
        {
            return await conn.GetAsync<Exercise>(id);
        }
    }
}
