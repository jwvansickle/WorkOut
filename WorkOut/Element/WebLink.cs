﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace WorkOut.Element
{
    public class WebLink : Label
    {
        public string Url
        {
            get { return (string)GetValue(UrlProperty); }
            set { SetValue(UrlProperty, value); }
        }

        public static readonly BindableProperty UrlProperty = BindableProperty.Create(
            "Url",
            typeof(string),
            typeof(WebLink),
            "",
            BindingMode.TwoWay);

        public WebLink()
        {
            // Default Styling
            TextColor = Color.FromHex("#4286f4");
            FontSize = 12;

            // Setup tap command
            GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = OpenLinkCommand
            });
        }

        private ICommand OpenLinkCommand
        {
            get
            {
                return new Command(() =>
                {
                    Device.OpenUri(new Uri(Url));
                });
            }
        }
    }
}
