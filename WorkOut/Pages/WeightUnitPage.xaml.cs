﻿using System;
using System.Collections.Generic;
using WorkOut.ViewModels;
using Xamarin.Forms;

namespace WorkOut.Pages
{
    public partial class WeightUnitPage : ContentPage
    {
        private readonly WeightUnitPageViewModel _viewModel;

        public WeightUnitPage()
        {
            _viewModel = new WeightUnitPageViewModel();

            BindingContext = _viewModel;

            InitializeComponent();
        }
    }
}
