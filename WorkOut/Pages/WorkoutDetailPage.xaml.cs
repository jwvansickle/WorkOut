﻿using System;
using System.Collections.Generic;
using WorkOut.ViewModels;
using Xamarin.Forms;
using static WorkOut.ViewModels.WorkoutsPageViewModel;

namespace WorkOut.Pages
{
    public partial class WorkoutDetailPage : ContentPage
    {
        private readonly WorkoutDetailPageViewModel _viewModel;

        /// <summary>
        /// DO NOT USE - for Preview only
        /// </summary>
        public WorkoutDetailPage()
        {
            InitializeComponent();

            _viewModel = new WorkoutDetailPageViewModel(null);

            BindingContext = _viewModel;
        }

        public WorkoutDetailPage(WorkoutVM workout)
        {
            InitializeComponent();

            _viewModel = new WorkoutDetailPageViewModel(workout);

            BindingContext = _viewModel;
        }
    }
}
