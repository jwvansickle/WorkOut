﻿using System;
using WorkOut.ViewModels;
using Xamarin.Forms;

namespace WorkOut.Pages
{
    public partial class AddExercisePage : ContentPage
    {
        private readonly AddExercisePageViewModel _viewModel;

        public AddExercisePage()
        {
            InitializeComponent();
        }

        public AddExercisePage(ExercisesPageViewModel viewModel)
        {
            _viewModel = new AddExercisePageViewModel(viewModel, Navigation);

            InitializeComponent();

            BindingContext = _viewModel;
        }
    }
}
