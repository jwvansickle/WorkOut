﻿using WorkOut.ViewModels;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;

namespace WorkOut.Pages
{
    public partial class EditExercisePage : ContentPage
    {
        public EditExercisePage()
        {
            InitializeComponent();
        }

        public EditExercisePage(ExerciseVM exerciseVM)
        {
            InitializeComponent();

            BindingContext = new EditExercisePageViewModel(exerciseVM, Navigation);
        }
    }
}
