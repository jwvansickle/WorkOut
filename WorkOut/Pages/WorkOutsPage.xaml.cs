﻿using System;
using WorkOut.ViewModels;
using Xamarin.Forms;
using static WorkOut.ViewModels.WorkoutsPageViewModel;

namespace WorkOut.Pages
{
    public partial class WorkOutsPage : ContentPage
    {
        private readonly WorkoutsPageViewModel _viewModel;

        public WorkOutsPage()
        {
            _viewModel = new WorkoutsPageViewModel(Navigation);

            InitializeComponent();

            BindingContext = _viewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await _viewModel.RefreshWorkouts();
        }

        private void OnItemSelected(object sender, EventArgs args)
        {
            if (workoutsList.SelectedItem != null)
            {
                var wo = workoutsList.SelectedItem as WorkoutVM;

                _viewModel.ItemSelectedCommand.Execute(wo);

                workoutsList.SelectedItem = null;
            }
        }
    }
}
