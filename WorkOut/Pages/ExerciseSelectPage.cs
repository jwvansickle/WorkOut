﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;

namespace WorkOut.Pages
{
    public class ExerciseSelectPage : SelectMultipleBasePage<ExerciseVM>
    {
        public ExerciseSelectPage(List<ExerciseVM> items) : base(items)
        {
            
        }
    }
}
