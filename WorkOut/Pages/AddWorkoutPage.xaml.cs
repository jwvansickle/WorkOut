﻿using WorkOut.ViewModels;
using Xamarin.Forms;

namespace WorkOut.Pages
{
    public partial class AddWorkoutPage : ContentPage
    {
        private readonly AddWorkoutPageViewModel _viewModel;

        public AddWorkoutPage()
        {
            _viewModel = new AddWorkoutPageViewModel(Navigation);

            InitializeComponent();

            BindingContext = _viewModel;
        }
    }
}
