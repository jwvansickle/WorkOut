﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkOut.Repositories;
using WorkOut.ViewModels;
using Xamarin.Forms;
using static WorkOut.ViewModels.ExercisesPageViewModel;

namespace WorkOut.Pages
{
    public partial class ExercisesPage : ContentPage
    {
        private readonly ExercisesPageViewModel _viewModel;

        public ExercisesPage()
        {
            _viewModel = new ExercisesPageViewModel();

            InitializeComponent();

            BindingContext = _viewModel;

            addExercise.Clicked += OnAddClicked;

            exercisesList.ItemSelected += OnItemSelected;
        }

        public async void OnAddClicked(object sender, EventArgs args)
        {
            var addPage = new AddExercisePage(_viewModel);

            await Navigation.PushAsync(addPage);
        }

        public async void OnDelete(object sender, EventArgs args)
        {
            if (((MenuItem)sender)?.CommandParameter is ExerciseVM exercise)
            {
                // Check that the exercise is not used in any workouts
                var workoutIds = await App.WorkoutRepo.Search(new WorkoutRepository.SearchParameters
                {
                    HasExercise = exercise.Id
                });

                if (workoutIds.ToList().Count > 0)
                {
                    await DisplayAlert("Unable to delete", "An exercise used in at least one workout may not be deleted.", "OK");
                    return;
                }

                IsBusy = true;
                try
                {
                    await App.ExerciseRepo.DeleteAsync(exercise.ToModel());
                }
                finally
                {
                    _viewModel.Exercises.Remove(exercise);
                    IsBusy = false;
                }
            }
        }

        public async void OnItemSelected(object sender, EventArgs args)
        {

            if (exercisesList.SelectedItem is ExerciseVM vm)
            {
                var editPage = new EditExercisePage(vm);

                await Navigation.PushAsync(editPage);

                exercisesList.SelectedItem = null;
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await _viewModel.RefreshExercises();
        }
    }
}
