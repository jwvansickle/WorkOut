﻿using System.Collections.Generic;
using SQLite;

namespace WorkOut.Models
{
    [Table("workout")]
    public class Workout
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(50), Indexed]
        public string Name { get; set; }

        [Ignore]
        public List<Exercise> Exercises { get; set; }
    }
}
