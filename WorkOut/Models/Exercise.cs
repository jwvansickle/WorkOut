﻿using SQLite;
namespace WorkOut.Models
{
    [Table("exercise")]
    public class Exercise
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(100), Unique]
        public string Name { get; set; }

        [Indexed]
        public short Repetitions { get; set; }

        [Indexed]
        public long TimeLength { get; set; }
    }
}
