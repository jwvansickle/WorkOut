﻿using System;
using SQLite;

namespace WorkOut.Models
{
    [Table("workout_exercise")]
    public class WorkoutExercise
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Indexed]
        public int ExerciseId { get; set; }

        [Indexed]
        public int WorkoutId { get; set; }
    }
}
